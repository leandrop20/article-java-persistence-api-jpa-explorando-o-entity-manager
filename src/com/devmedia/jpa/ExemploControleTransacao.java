package com.devmedia.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.devmedia.jpa.entity.Pessoa;
import com.devmedia.jpa.util.JPAUtil;

public class ExemploControleTransacao {

	public static void main(String[] args) {
		EntityManager em = JPAUtil.em();
		EntityTransaction tx = null;
		
		try {
			tx = em.getTransaction();
			tx.begin();
			
			Pessoa pessoa = new Pessoa();
			pessoa.setNome("Andrei");
			
			Pessoa pessoa2 = new Pessoa();
			pessoa2.setNome("Maria");
			
			em.persist(pessoa);
			em.persist(pessoa2);
			
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null && tx.isActive()) {
				tx.rollback();
			}
		}
		
		Query q = em.createQuery("select p from Pessoa p", Pessoa.class);
		
		List<Pessoa> list = q.getResultList();
		for (Pessoa p : list) {
			System.out.println(p);
		}
		
		em.close();
	}
	
}