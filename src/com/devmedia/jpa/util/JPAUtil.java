package com.devmedia.jpa.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("exemplo-pu");
	
	public static EntityManager em() {
		return emf.createEntityManager();
	}
	
}